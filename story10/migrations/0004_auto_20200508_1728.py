# Generated by Django 3.0.5 on 2020-05-08 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story10', '0003_profile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birth_date',
            field=models.DateField(null=True),
        ),
    ]
