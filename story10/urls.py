from django.urls import include, path
from . import views



urlpatterns = [
    path('', views.index, name = "index"),
    path('unknown/',views.unknown, name="unknown"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signup/', views.signup, name = "signup")

]