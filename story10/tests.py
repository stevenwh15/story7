from django.test import TestCase,override_settings
from django.test import Client
from django.urls import resolve, reverse
from django.test import LiveServerTestCase
from .views import index, unknown, signup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class LoginUnitTest(TestCase):
   def test_story10_url_is_exist(self):
      response = Client().get('/story10/')
      self.assertEqual(response.status_code,200)

   def test_story10_url_is_exist(self):
      response = Client().get('/story10/accounts/login/')
      self.assertEqual(response.status_code,200)

   def test_story10_url_unknown_is_exist(self):
      response = Client().get('/story10/unknown/')
      self.assertEqual(response.status_code,200)

   def test_story10_funct(self):
      found = resolve('/story10/')
      self.assertEqual(found.func,index)
   
@override_settings(DEBUG=True)
class LoginFunctionalTest(StaticLiveServerTestCase):
   def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super().setUp()

   def tearDown(self):
        self.browser.quit()
        super().tearDown()
   
   def test_is_authenticated(self):
      self.browser.get(f"{self.live_server_url}/story10/")
      self.browser.implicitly_wait(5)

      self.assertIn("Unknown", self.browser.title)
