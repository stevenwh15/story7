from django.shortcuts import render,redirect
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
import json

def index(req):
    if req.user.is_authenticated:
        context = {
            'user': req.user
        }
        return render(req, 'story10/index.html', context)
    else :
        return redirect('unknown')

def unknown(req):
    return render(req, 'story10/unknown.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    return render(request, 'story10/signup.html', {'form': form})


