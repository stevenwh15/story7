from django.shortcuts import render, get_object_or_404
from django.forms.models import model_to_dict
from .models import Books
import json
from django.http import JsonResponse

def books_funct(req):
    return render(req, "story9/index.html")

def liked_books(req):
    if req.method == "POST":
        json_data = json.loads(req.body)

        print(json_data)
        id_books = json_data['id_books']

        del json_data['id_books']

        obj, created = Books.objects.update_or_create(id_books = id_books, defaults=json_data)

        return JsonResponse({'success': True, 'book': model_to_dict(obj)}) 
    else:
        books = Books.objects.order_by('-likes')

        return JsonResponse({'books': list(books.values())})