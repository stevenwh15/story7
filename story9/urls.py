from django.urls import include, path
from .views import books_funct, liked_books

urlpatterns = [
    path('', books_funct, name= 'story9'),
    path('api/liked', liked_books, name='liked-books')
]