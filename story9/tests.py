from django.test import TestCase,override_settings
from django.test import Client
from django.urls import resolve, reverse
from django.test import LiveServerTestCase
from .views import books_funct
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class StatusUnitTest(TestCase):

     def test_story8_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)

     def test_story8_funct(self):
        found = resolve('/story9/')
        self.assertEqual(found.func,books_funct)

@override_settings(DEBUG=True)
class StatusFunctionalTest(StaticLiveServerTestCase):
   def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super().setUp()

   def tearDown(self):
        self.browser.quit()
        super().tearDown()
   
   def test_searchbox(self):
      self.browser.get(f"{self.live_server_url}/story9/")
      self.browser.implicitly_wait(5)

      self.assertIn("Library", self.browser.title)

      search_box = self.browser.find_element_by_id('search')
      search_box.send_keys('abcd')
      search_box.send_keys(Keys.ENTER)
      time.sleep(3)

      page_text = self.browser.find_element_by_tag_name('body').text
      self.assertIn("An Introductory Book into the English Alphabet", page_text)
      self.assertIn("Makwei Mabioor Deng", page_text)

   def test_like_button(self):
      self.browser.get(f"{self.live_server_url}/story9/")
      self.browser.implicitly_wait(5)

      search_box = self.browser.find_element_by_id('search')
      search_box.send_keys('abcd')
      search_box.send_keys(Keys.ENTER)
      time.sleep(5)

      like_button = self.browser.find_element_by_css_selector("[data-book-id='QKfkvri-4KgC']")
      like_count = self.browser.find_element_by_css_selector("#QKfkvri-4KgC-likes").text
      like_button.click()
      
      like_count_new = self.browser.find_element_by_css_selector("#QKfkvri-4KgC-likes").text
      self.assertNotEqual(int(like_count_new), int(like_count))


