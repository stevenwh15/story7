from django.db import models

class Books(models.Model):
    id_books = models.CharField(max_length=32, primary_key=True)
    title = models.CharField(max_length = 64)
    author = models.CharField(max_length = 64)
    publisher = models.CharField(max_length = 64)
    image = models.URLField()
    likes = models.IntegerField(default=0)
