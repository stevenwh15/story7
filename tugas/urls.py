from django.urls import include, path
from .views import get_confirmation, get_all

urlpatterns = [
    path('all/', get_all, name='all'),
    path('confirm/', get_confirmation, name='confirm'),
]