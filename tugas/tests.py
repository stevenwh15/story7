from django.test import TestCase,override_settings
from django.test import Client
from django.urls import resolve, reverse
from django.test import LiveServerTestCase
from .views import get_all, get_confirmation
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class StatusUnitTest(TestCase):

    def setUp(self):
        self.test_status = Status.objects.create(nama= "Test", pesan= "test123")

    def test_all_url_is_exist(self):
        response = Client().get('/all/')
        self.assertEqual(response.status_code,200)

    def test_using_get_all_funct(self):
        found = resolve('/all/')
        self.assertEqual(found.func,get_all)

    def test_confirm_url_is_exist(self):
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code,200)
    
    def test_using_get_confirm_funct(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func,get_confirmation)

    def test_all_add_post_success_and_render_the_result(self):
        test = 'tes'
        response_post = Client().post('/all/', {'nama': 'tes', 'pesan': 'testing'})
        self.assertEqual(response_post.status_code, 200)

class StatusFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super().setUp()

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
    def test_input_form_and_confirm_data(self):
        self.browser.get(self.live_server_url + '/all/')
        nama = self.browser.find_element_by_name('nama')
        pesan = self.browser.find_element_by_name('pesan')
        button = self.browser.find_element_by_id('btn')

        nama.send_keys('Test')
        pesan.send_keys('123')
        button.click()

        button_ok = self.browser.find_element_by_id('btn2')
        button_ok.click()

        self.assertIn('Test', self.browser.page_source)
        self.assertIn('123', self.browser.page_source)


        



