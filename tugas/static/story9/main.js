let fetchedBooks = {};

async function query(q) {
  const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=${q}`;

  const data = await (await fetch(apiUrl)).json();

  const result = data.items;

  if (data) {
    $("#result").html(
      result
        .map((item) => {
          const { id } = item;
          const { title, authors, imageLinks, publisher } = item.volumeInfo;

          // add book to list of fetched books
          if (!fetchedBooks.hasOwnProperty(id)) {
            fetchedBooks[id] = {
              title,
              author: authors ? authors.join(", ") : "Unknown",
              image: imageLinks?.thumbnail,
              publisher: publisher ? publisher : "Unknown",
              likes: 0,
            };
          }

          return ` 
            <div class="book">
            <div class="card jus row justify-content-center mt-2 mr-5 ml-5" >
            <div class="container">
            <div class="row ml-3 mt-3 mb-3">
                <div class="row d-flex align-items-center" class="gambar">
                <img
                    src="${imageLinks?.thumbnail}"
                    alt="${title}"
                    width="128"
                    height="170"
                />
                </div>
                <div class="ml-5 mt-4 col-md-9" id="data">
                <p>Title : ${title}</p>
                <p>Author : ${authors ? authors.join(", ") : "Unknown"}</p>
                <p>Publisher : ${publisher ? publisher : "Unknown"}</p>
                <p>Likes : <span id="${id + "-likes"}">${
            fetchedBooks[id].likes
          }</span></p>
                <div class="d-flex justify-content-start">
                <button id="button_like" type="button" class="btn btn-outline-primary like-button" data-book-id="${id}" onclick="like('${id}')">LIKE</button>
                </div>
                </div>
            </div>
            </div>
            </div>
            </div>
            `;
        })
        .join("\n")
    );
  } else {
    $("#result").html("Tidak ditemukan!");
  }
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

async function getLikedBooks() {
  const APIUrl = "/story9/api/liked";

  const data = await (await fetch(APIUrl)).json();

  data.books.forEach((book) => {
    const { id_books, ...bookData } = book;
    fetchedBooks[id_books] = bookData;
  });

  console.log(fetchedBooks);

  $("#liked-books").html(
    data.books
      .slice(0, 5)
      .map((book) => {
        const { title, author, image, publisher, likes } = book;

        return ` 
            <div class="book">
            <div class="card jus row justify-content-center mt-2 mr-5 ml-5" >
            <div class="container">
            <div class="row ml-3 mt-3 mb-3">
                <div class="row d-flex align-items-center" class="gambar">
                <img
                    src="${image}"
                    alt="${title}"
                    width="128"
                    height="170"
                />
                </div>
                <div class="ml-5 mt-4 col-md-9" id="data">
                <p>Title : ${title}</p>
                <p>Author : ${author}</p>
                <p>Publisher : ${publisher}</p>
                <p>Likes : <span>${likes}</span></p>
                </div>
            </div>
            </div>
            </div>
            </div>
            `;
      })
      .join("\n")
  );
}

$("document").ready(function () {
  getLikedBooks();

  $("#searchform").submit(async (e) => {
    e.preventDefault();

    $("#result").html("Loading...");

    query($("#search").val());
  });
});

const like = async (id) => {
  const url = "/story9/api/liked";

  const book = fetchedBooks[id];
  book.likes = book.likes + 1;

  // optimistic update of like counts
  const likesElementId = "#" + id + "-likes";
  $(likesElementId).text(book.likes);

  // prepare payload
  const payload = {
    id_books: id,
    ...book,
  };

  const csrftoken = getCookie("csrftoken");

  const response = await fetch(url, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
    headers: { "X-CSRFToken": csrftoken },
  });

  const result = await response.json();

  console.log(result);

  await getLikedBooks();
};
