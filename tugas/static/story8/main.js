$(document).ready(() => {
    const $identitas = $('#identitas');
    const $pengalaman = $('#pengalaman');
    const $prestasi = $('#prestasi');
    const $aktivitas = $('#aktivitas');
    const $isi_identitas = $('#isi_identitas');
    const $isi_pengalaman = $('#isi_pengalaman');
    const $isi_prestasi = $('#isi_prestasi');
    const $isi_aktivitas = $('#isi_aktivitas');

    $identitas.on('click',() =>{
        console.log("masuk");
        $isi_identitas.toggle();
    })
    $pengalaman.on('click',() =>{
        console.log("masuk");
        $isi_pengalaman.toggle();
    })
    $prestasi.on('click',() =>{
        console.log("masuk");
        $isi_prestasi.toggle();
    })
    $aktivitas.on('click',() =>{
        console.log("masuk");
        $isi_aktivitas.toggle();
    })

    const $posisi_identitas = $('.identitas');
    const $posisi_pengalaman = $('.pengalaman');
    const $posisi_prestasi = $('.prestasi');
    const $posisi_aktivitas = $('.aktivitas');

    $('#identitas_up').on('click',()=>{
        $posisi_identitas.prev().insertAfter($posisi_identitas);
    })
    $('#identitas_down').on('click',()=>{
        $posisi_identitas.next().insertBefore($posisi_identitas);
    })
    $('#pengalaman_up').on('click',()=>{
        $posisi_pengalaman.prev().insertAfter($posisi_pengalaman);
    })
    $('#pengalaman_down').on('click',()=>{
        $posisi_pengalaman.next().insertBefore($posisi_pengalaman);
    })
    $('#prestasi_up').on('click',()=>{
        $posisi_prestasi.prev().insertAfter($posisi_prestasi);
    })
    $('#prestasi_down').on('click',()=>{
        $posisi_prestasi.next().insertBefore($posisi_prestasi);
    })
    $('#aktivitas_up').on('click',()=>{
        $posisi_aktivitas.prev().insertAfter($posisi_aktivitas);
    })
    $('#aktivitas_down').on('click',()=>{
        $posisi_aktivitas.next().insertBefore($posisi_aktivitas);
    })

    $('#theme').on('click',() =>{
        $("body").toggleClass("pink");
        $(".card-title").toggleClass("pink_isi");
        $(".card-body").toggleClass("pink_isi");
    })
})