from django.db import models

class Status(models.Model):
    nama = models.CharField(max_length=32)
    pesan = models.TextField(blank= True, verbose_name="Pesan/Status")