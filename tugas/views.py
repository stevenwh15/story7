from django.shortcuts import render
from .models import Status
from .form import tambah_status

def get_all(req):
    if req.method == "POST":
        all_status = Status.objects.all()
        form_status = tambah_status(req.POST)
        Status.objects.create(
            nama = req.POST['nama'],
            pesan = req.POST['pesan']
        )
        return render(req, "story7/all.html", {'all_status': all_status, 'form_status': form_status})
    else:
        all_status = Status.objects.all()
        form_status = tambah_status()   
        return render(req, "story7/all.html", {'all_status': all_status, 'form_status': form_status})

def get_confirmation(req):
    if req.method == "POST":
        isi = {
        'nama': req.POST['nama'],
        'pesan': req.POST['pesan'],
        }
        return render(req, "story7/confirm.html",isi)
    else:
        return render(req, "story7/confirm.html")