from django.test import TestCase,override_settings
from django.test import Client
from django.urls import resolve, reverse
from django.test import LiveServerTestCase
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class StatusUnitTest(TestCase):

     def test_story8_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)

     def test_story8_funct(self):
        found = resolve('/story8/')
        self.assertEqual(found.func,index)

@override_settings(DEBUG=True)
class StatusFunctionalTest(StaticLiveServerTestCase):
   def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super().setUp()

   def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
   def test_accordion_down_change(self):
        self.browser.get(f"{self.live_server_url}/story8/")

        elem = self.browser.find_elements_by_css_selector('#accordion > div')[1]
        elem_button = elem.find_elements_by_css_selector('.button > button')[0]

        elem_button.click()

        new_elem = self.browser.find_elements_by_css_selector('#accordion > div')[2]
        self.assertEqual(elem.find_element_by_tag_name('h4').text, new_elem.find_element_by_tag_name('h4').text)

   def test_accordion_up_change(self):
        self.browser.get(f"{self.live_server_url}/story8/")

        elem = self.browser.find_elements_by_css_selector('#accordion > div')[1]
        elem_button = elem.find_elements_by_css_selector('.button > button')[1]

        elem_button.click()

        new_elem = self.browser.find_elements_by_css_selector('#accordion > div')[0]
        self.assertEqual(elem.find_element_by_tag_name('h4').text, new_elem.find_element_by_tag_name('h4').text)
   
   def test_theme_change(self):
      self.browser.get(self.live_server_url + '/story8/')
      css1 = self.browser.find_element_by_css_selector("body")
      button_change = self.browser.find_element_by_id("theme")
      button_change.click()
      css2 = self.browser.find_element_by_css_selector(".pink")
      self.assertEqual(css1,css2)